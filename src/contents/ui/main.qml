// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Intralexical <Intralexical@gmail.com>

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.GlobalDrawerZOrder 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("GlobalDrawerZOrder")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: App.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    Component.onCompleted: App.restoreWindowGeometry(root)

    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    property int counter: 0

    globalDrawer: Kirigami.GlobalDrawer {
        title: i18n("GlobalDrawerZOrder")
        titleIcon: "applications-graphics"
        isMenu: false

        modal: false
        collapsible: true
        collapsed: true

        showContentWhenCollapsed: true
        actions: [
            Kirigami.Action {
                text: i18n("Plus One")
                icon.name: "list-add"
                onTriggered: {
                    counter += 1
                }
            },
            Kirigami.Action {
                text: i18n("About GlobalDrawerZOrder")
                icon.name: "help-about"
                onTriggered: pageStack.layers.push('qrc:About.qml')
            },
            Kirigami.Action {
                text: i18n("Quit")
                icon.name: "application-exit"
                onTriggered: Qt.quit()
            }
        ]
        Controls.Button {
            text: "RMB"
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.RightButton
                onClicked: (event) => {
                    console.log("Keep right clicking this until the menu shows up underneath the drawer. You might need maybe over a dozen clicks or so.")
                    _testMenu.x = event.x
                    _testMenu.y = event.y
                    _testMenu.open()
                }
            }
            Controls.Menu {
                id: _testMenu
                Controls.MenuItem {
                    text: "Keep right clicking"
                }
            }

        }
    }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: page

    Kirigami.Page {
        id: page

        Layout.fillWidth: true

        title: i18n("Main Page")

        actions.main: Kirigami.Action {
            text: i18n("Plus One")
            icon.name: "list-add"
            tooltip: i18n("Add one to the counter")
            onTriggered: {
                counter += 1
            }
        }

        ColumnLayout {
            width: page.width

            anchors.centerIn: parent

            Controls.Label {
                Layout.alignment: Qt.AlignCenter
                text: "1. Right click on the button that says \"RMB\" at the bottom of the GlobalDrawer{}.\n\n2. When the Menu{} is open, move your mouse left or up a bit, off of the Menu{}.\n\n3. Left or right click on either the button or the area around it to close the Menu{}.\n\n4. Do this repeatedly.\n\n5. Eventually, the GlobalDrawer{} will seem to gain focus on click, and move to on top of the Menu{}.\n\n6. Once this happens, all subsequent times opening the Menu{} will also show it underneath the drawer."
            }

            Controls.Button {
                Layout.alignment: Qt.AlignHCenter
                text: "+ 1"
                onClicked: counter += 1
            }
        }
    }
}
