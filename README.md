When a `GlobalDrawer{}` contains items that open a `Controls.Menu{}` on right click, and you keep repeatedly right clicking on those items to close and open that menu, eventually the `GlobalDrawer{}` will seem to gain some kind of focus and the `Menu{}` will begin to show up underneath the `GlobalDrawer{}` instead of on top of it.

See the attached screenshots.

https://bugs.kde.org/show_bug.cgi?id=465154
